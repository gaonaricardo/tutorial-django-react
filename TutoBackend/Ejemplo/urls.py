from django.urls import path, include
from .views import TaskList, TaskDetail, TaskViewset #get_tasks
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('tareas', TaskViewset, basename='tareas')

urlpatterns = [
    path('tasks/', TaskList.as_view()),
    path('tasks/<int:pk>/', TaskDetail.as_view()),
    path('api/', include(router.urls)),
]
