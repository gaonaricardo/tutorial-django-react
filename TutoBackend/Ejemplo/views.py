from django.shortcuts import get_object_or_404, render
from .models import Task
from .serializers import TaskSerializer
from rest_framework.decorators import api_view
from rest_framework import response, status
from rest_framework import views, viewsets, permissions

# Create your views here.
"""
@api_view(['GET'])
def get_tasks(req): 
    tasks = Task.objects.all()
    serializer = TaskSerializer(tasks, many=True)
    return response.Response(data=serializer.data, status=status.HTTP_200_OK)
"""

#Apiview trabaja sobre verbos http
class TaskList(views.APIView):
    def get(self, req):
        #Traer todas las tareas
        tasks = Task.objects.all()
        #Serializar las tareas 
        serializer = TaskSerializer(tasks, many=True)
        #Retornar un Response
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, req):
        serializer = TaskSerializer(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return response.Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class TaskDetail(views.APIView):
    def get(self, req, pk):
        #Traer todas las tareas
        task = get_object_or_404(Task, pk=pk)
        #Serializar las tareas 
        serializer = TaskSerializer(task)
        #Retornar un Response
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def delete(self, req, pk):
        task = Task.objects.get(pk=pk)
        task.delete()
        return response.Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, req, pk):
        task = Task.objects.get(pk=pk)
        serializer = TaskSerializer(task, data=req.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(data=serializer.data, status=status.HTTP_200_OK)
        return response.Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

class TaskViewset(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [permissions.IsAuthenticated]
