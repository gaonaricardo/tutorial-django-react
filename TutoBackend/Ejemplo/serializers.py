from rest_framework import serializers
from .models import Task
"""
class TaskSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.TextField()
    done = serializers.BooleanField()
"""
class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"
        #fields = ("title", "description")